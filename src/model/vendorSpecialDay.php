<?php

namespace Model;

class VendorSpecialDay {
    
    /**
     * @var int 
     */
    private $id;

    /**
     * @var int 
     */
    private $vendor_id;

    /**
     * @var string 
     */
    private $special_date;

    /**
     * string
     */
    private $event_type;

    /**
     * @var int 
     */
    private $all_day;

    /**
     * @var string | NULL 
     */
    private $start_hour;

    /**
     * @var string | NULL 
     */
    private $stop_hour;

    /**
     * @param int
     */
    public function setId(int $id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getVendorId(): int {
        return $this->vendor_id;
    }

    /**
     * @param int $vendor_id
     */
    public function setVendor_id(int $vendor_id) {
        $this->vendor_id = $vendor_id;
    }

    /**
     * @return string
     */
    public function getSpecialDate(): string {
        return $this->special_date;
    }

    /**
     * @param string $special_date
     */
    public function setSpecialDate(string $special_date) {
        $this->special_date = $special_date;
    }

    /**
     * @param string $event_type
     */
    public function setEvent_type(string $event_type) {
        $this->event_type = $event_type;
    }

    /**
     * @return string
     */
    public function getEvent_type(): string {
        return $this->event_type;
    }

    /**
     * @return int
     */
    public function getAllDay(): int {
        return $this->all_day;
    }

    /**
     * @param int $all_day
     */
    public function setAllDay(int $all_day) {
        $this->all_day = $all_day;
    }

    /**
     * @return string | NULL
     */
    public function getStartHour() {
        return $this->start_hour;
    }

    /**
     * @param string | NULL $start_hour
     */
    public function setStartHour($start_hour) {
        $this->start_hour = $start_hour;
    }

    /**
     * @return string | NULL
     */
    public function getStopHour() {
        return $this->stop_hour;
    }

    /**
     * @param string | NULL $stop_hour
     */
    public function setStopHour($stop_hour) {
        $this->stop_hour = $stop_hour;
    }

    /**
     * gets the weekday of vendor special date
     * @return int
     */
    public function getWeekDay(): int {
        return (int) date('N', strtotime($this->getSpecialDate()));
    }

}
