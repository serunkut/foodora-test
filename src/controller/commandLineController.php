<?php

namespace Controller;

/**
 * CommandLineController
 * @author erunkut
 */
class CommandLineController {

    private $commandLineService;
    private $patchService;
    private $controllerActions;
    private $connection;
    private $mainView;

    public function __construct(\DAO\DatabaseInterface $db, \Controller\Actions $controllerActions, \View\MainView $view) {
        $this->connection = $db;
        $this->controllerActions = $controllerActions;
        $this->commandLineService = new \Service\CommandLineService(getopt("afri"));
        $this->patchService = new \Service\PatchService(new \DAO\VendorScheduleDAO($this->connection), new \DAO\VendorSpecialDayDAO($this->connection));
        $this->mainView = $view;
    }

    /**
     * checks the cli input parameters and converts them to actions
     * these actions are then applied 
     */
    public function initiate() {
        try {
            $result = $this->commandLineService->checkCliCommands($this->controllerActions);
            $this->applyActions($result);
        } catch (\Throwable $t) {
            $this->commandLineService->handleError($t);
        }
    }

    /**
     * central function for action handling
     * these actions are standardized (constants) and can be use from various interfaces like cli, gui, etc.
     * 
     * @param \Controller\Actions $actions
     * @param \View\MainView $mainView
     * @throws \Exception
     */
    public function applyActions(\Controller\Actions $actions) {
        try {
            foreach ($actions->getActions() as $action) {
                $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' action: ' . $action);
                switch ($action) {
                    case $actions::ACTION_DELETE_BACKUP:
                        $result = $this->patchService->deleteBackupTable();
                        break;
                    case $actions::ACTION_CREATE_BACKUP:
                        $result = $this->patchService->createBackupTable();
                        break;
                    case $actions::ACTION_APPLY_PATCH:
                        $result = $this->patchService->applyPatch();
                        break;
                    case $actions::ACTION_REVERT_PATCH:
                        $result = $this->patchService->revertPatch();
                        break;
                    case $actions::ACTION_DISPLAY_HELP:
                        $result = $this->mainView->displayhelp();
                        break;
                    case $actions::ACTION_CHECK_BACKUP_TABLE_EXISTS:
                        $result = $this->patchService->checkBackupTableVendorScheduleExits();
                        break;
                    default:
                        throw new \Exception("no action received");
                }
                if (!empty($result)) {
                    $this->mainView->displayText($result);
                }
            }
        } catch (\Throwable $t) {
            $this->commandLineService->handleError($t);
        }
    }
}
