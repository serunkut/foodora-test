+++++++++++++++++++++ Foodora Test +++++++++++++++++++++ 

This application runs/tested with PHP version 7.0.14
(if you want to simulate the environment use this docker repo: https://github.com/osteel/docker-tutorial-2.git)

Before running the application database connection parameters must be set in config/config.php
All errors that implement the Throwable interface (http://php.net/manual/en/class.throwable.php) 
are written into the log files.
Log files will be created in the directory src/log/

Start the application from its root directory (foodora-test/) with: php run.php 
and display the instruction:
                 -a   apply patch: backups the vendor_schedule table if a backup does not exists and applies the patch, is idempotent
                 -f   vendor_schedule backup table will be deleted and created again, can only be combined with -a
                 -r   reverts the applied patch

Example to run the script: php run.php -a

+++++++++++++++++++++ Workflow & Algorithm  +++++++++++++++++++++

Apply patch workflow (-a): 
1) If backup table exits and has records, data from vendor_schedule is NOT backed up again (use -f) else 
a backup table table is created and filled with data from vendor_schedule.
2) vendor_schedule data is merged with data from vendor_special_day and => Algorithm (see also in code docu):
If for the weekday a Vendor Special Day Entry(s) exists, it uses that/them instead of the Vendor Schedule,
if there is no Vendor Special Day Entry(s), the Vendor Schedule Entry(s) for that weekday is/are used.

Revert workflow (-r):
deletes all records from vendor_schedule table
inserts the backup data from vendor_schedule_backup_special_day_fix and drops this table

Troubleshooting: 
- If for some reason the vendor schedule backup table (vendor_schedule_backup_special_day_fix) is not dropped 
and the patch must be applied again with a updated vendor_schedule table: either use the -f option or delete it manually
- Application displays some messages to stdout that are defined in config.php

+++++++++++++++++++++ DB Row Count & Runtime  +++++++++++++++++++++

DB Row Count:

Tested with
5678 vendors,
39000 vendor_schedule,
32000 vendor_special_day 
rows

Runtime:

Apply Patch:
Algorithm takes about 8 Minutes (+10 seconds for insert operation).

Revert Patch
takes about less than a second. 


+++++++++++++++++++++ External Libraries/Classes +++++++++++++++++++++

Composer is not needed everything is included into the source
 
Logging: 
External log class Logger author is Kenny Katzgrau, 
this log class uses the PSR Log Interface, all further information can be found in the directory src/log/.
Log level can be set in config.php

Autoloader:
http://www.php-fig.org/psr/psr-4/examples/