<?php

namespace View;

/**
 * MainView
 * @author erunkut
 */
class MainView {

    /**
     * displays the given string as text
     * @param string $text
     */
    public function displayText(string $text) {
        echo $text . PHP_EOL;
    }

    /**
     * returns the help text for the cli app
     * 
     * @return string
     */
    public function displayHelp() {
        return PHP_EOL . "INSTRUCTIONS:" . PHP_EOL
                . "-a   apply patch: backups the vendor_schedule table if a backup does not exists and applies the patch, is idempotent" . PHP_EOL
                . "-f   vendor_schedule backup table will be deleted and created again, can only be combined with -a" . PHP_EOL
                . "-r   reverts the applied patch" . PHP_EOL;
    }

}
