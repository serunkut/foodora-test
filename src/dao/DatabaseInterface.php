<?php

namespace DAO;

/**
 * DatabaseInterface
 * @author erunkut
 */
interface DatabaseInterface {

    public function executeStatement(string $query, array $values = NULL, $fetchMethod = NULL, $objectType = NULL);

    public function insertRows(string $query, array $assocArray2d);
}
