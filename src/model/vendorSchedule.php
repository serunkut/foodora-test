<?php

namespace Model;

class VendorSchedule {

    /**
     * @var int 
     */
    private $id;

    /**
     * @var int 
     */
    private $vendor_id;

    /**
     * @var int 
     */
    private $weekday;

    /**
     * @var int 
     */
    private $all_day;

    /**
     * @var string | NULL 
     */
    private $start_hour;

    /**
     * @var string | NULL
     */
    private $stop_hour;

    /**
     * @param int
     */
    public function setId(int $id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getVendorId(): int {
        return $this->vendor_id;
    }

    /**
     * @param int $vendor_id
     */
    public function setVendor_id(int $vendor_id) {
        $this->vendor_id = $vendor_id;
    }

    /**
     * @return int
     */
    public function getWeekDay(): int {
        return $this->weekday;
    }

    /**
     * @param int $week_day
     */
    public function setWeekDay(int $week_day) {
        $this->weekday = $week_day;
    }

    /**
     * @return int
     */
    public function getAllday(): int {
        return $this->all_day;
    }

    /**
     * @param int $all_day
     */
    public function setAllday(int $all_day) {
        $this->all_day = $all_day;
    }

    /**
     * @return string | NULL
     */
    public function getStartHour() {
        return $this->start_hour;
    }

    /**
     * @param string | NULL $start_hour
     */
    public function setStartHour($start_hour) {
        $this->start_hour = $start_hour;
    }

    /**
     * @return string | NULL
     */
    public function getStopHour() {
        return $this->stop_hour;
    }

    /**
     * @param string | NULL $stop_hour
     */
    public function setStopHour($stop_hour) {
        $this->stop_hour = $stop_hour;
    }
    
//    public function unsetID(){
//        unset($this->id)
//    }

}
