<?php

namespace Service;

/**
 * PatchService
 * @author erunkut
 */
class PatchService {

    private $vendorScheduleDAO;
    private $vendorSpecialDayDAO;

    function __construct(\DAO\VendorScheduleDAO $vendorScheduleDAO, \DAO\VendorSpecialDayDAO $vendorSpecialDayDAO) {
        $this->vendorScheduleDAO = $vendorScheduleDAO;
        $this->vendorSpecialDayDAO = $vendorSpecialDayDAO;
    }

    /**
     * check if the backup table for vendor_schedule table exists
     * 
     * @return bool TRUE if table exists else FALSE
     */
    public function checkBackupTableVendorScheduleExits(): bool {
        return $this->vendorScheduleDAO->checkIfBackupTableExists();
    }

    /**
     * check if the backup table for vendor_schedule table has values
     * 
     * @return bool TRUE if table exists else FALSE
     * 
     */
    public function checkBackupTableHasValues(): bool {
        return $this->vendorScheduleDAO->checkIfBackupTableHasValues();
    }

    /**
     * creates the backup table
     * 
     * @return string a message
     */
    public function createBackupTable() {
        $this->vendorScheduleDAO->createBackupTableVendorSchedule();
        $this->vendorScheduleDAO->insertDataIntoBackupTableVendorSchedule();
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . 'backup table with data created');
        return BACKUP_TABLE_CREATED_WITH_DATA;
    }

    /**
     * deletes the backup table
     * 
     * @return string
     */
    public function deleteBackupTable() {
        $backupTableExists = $this->checkBackupTableVendorScheduleExits();
        if ($backupTableExists === TRUE) {
            $result = $this->vendorScheduleDAO->dropBackupTableVendorSchedule();
            if ($result === TRUE) {
                return BACKUP_TABLE_DELETED;
            }
        } else {
            return BACKUP_TABLE_NOT_EXISTS;
        }
    }

    /**
     * reverts the applied patch
     * truncates the vendor schedule table
     * inserts data from backup table
     * deletes the backup table
     * 
     * @return string
     */
    public function revertPatch() {
        $backupTableExists = $this->checkBackupTableVendorScheduleExits();
        if ($backupTableExists === FALSE) {
            $GLOBALS["log"]->info(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . 'backup table missing, reverting failed ');
            return BT_MISSING_REVERT_FAILED;
        }
        $this->vendorScheduleDAO->revertPatch($this->vendorSpecialDayDAO);
        $GLOBALS["log"]->info(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' reverting success ');
        return REVERT_SUCCESS;
    }

    /**
     * creates if necessary the backup table
     * delete all data from the vendor_schedule table and
     * inserts the new data into vendor_schedule
     * @returns string
     */
    public function applyPatch() {
        //create backup table if it does not exists
        $this->handleBackupTable();
        //load data from vendor_special_day table into objects
        $vendorSpecialDayObjects = $this->vendorSpecialDayDAO->getVendorSpecialDaysBetwennDates(START_DATE, END_DATE);
        //load data from vendor_schedule table into objects
        $vendorScheduleObjects = $this->vendorScheduleDAO->getVendorSchedules();
        //get vendor ids
        $vendorIds = $this->vendorScheduleDAO->getVendorIDs();
        //drop vendor_schedule table
        //create the new schedules
        $newSchedules = $this->getNewSchedules($vendorScheduleObjects, $vendorSpecialDayObjects, $vendorIds);
        //convert the objects into an 2d assoc array
        $assocArrayNewSchedules = $this->objectArrayToAssoc($newSchedules);
        //insert the new data into vendor_schedule
        $this->vendorScheduleDAO->purgeInsertIntoVendorSchedule($assocArrayNewSchedules);
        $GLOBALS["log"]->info(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' applying the patch was a success ');
        return PATCH_APPLIED;
    }

    /**
     * Creates the new Vendor Schedules
     * 
     * @param array $vendorScheduleObjects
     * @param array $vendorSpecialDayObjects
     * @param array $vendorIds
     * @return array of \Model\VendorSchedule Objects
     */
    private function getNewSchedules(array $vendorScheduleObjects, array $vendorSpecialDayObjects, array $vendorIds): array {
        $finalArray = [];
        foreach ($vendorIds as $vendorId) {
            $newScheduleForVendor = $this->newVendorSchedules($vendorId, $vendorScheduleObjects, $vendorSpecialDayObjects);
            $finalArray = array_merge($finalArray, $newScheduleForVendor);
        }
        return $finalArray;
    }

    /**
     * Overwrites the Vendor Schedules with the Vendor Special Days for the given $vendorId
     * 
     * If for the weekday a Vendor Special Day Entry(s) exists, it uses that/them instead of the Vendor Schedule,
     * if there is no Vendor Special Day Entry(s), the Vendor Schedule Entry(s) for that weekday is/are used
     * 
     * @param int $vendorId
     * @param array $vendorScheduleObjects \Model\VendorSchedule
     * @param array $vendorSpecialDayObjects \Model\VendorSpecialDay
     * @return array of \Model\VendorSchedule Objects Objects
     */
    private function newVendorSchedules(int $vendorId, array $vendorScheduleObjects, array $vendorSpecialDayObjects): array {
        $newVendorSchedules = [];
        for ($i = 1; $i < 8; $i++) {
            //get vendor schedule objects with specified vendor_id and weekday
            $vendorScheduleObjectsWeekday = $this->getObjectsFromArrayByValues($vendorScheduleObjects, $vendorId, $i);
            //get vendor schedule special day objects with specified vendor_id and weekday
            $vendorSpecialDayObjectsWeekday = $this->getObjectsFromArrayByValues($vendorSpecialDayObjects, $vendorId, $i);
            // if there are vendor_special_day for the weekday put it into the $newVendorSchedules array
            if (!empty(($vendorSpecialDayObjectsWeekday))) {
                foreach ($vendorSpecialDayObjectsWeekday as $vendorSpecialDayObject) {
                    $tempVendorScheduleObject = $this->createNewVendorScheduleObject($vendorSpecialDayObject);
                    array_push($newVendorSchedules, $tempVendorScheduleObject);
                }
                //vendor_special_day is empty but there are vendor_schedules for this weekday put them into the $newVendorSchedules array
            } elseif (!empty(($vendorScheduleObjectsWeekday))) {
                foreach ($vendorScheduleObjectsWeekday as $vendorScheduleObject) {
                    array_push($newVendorSchedules, $vendorScheduleObject);
                }
            }
        }
        return $newVendorSchedules;
    }

    /**
     * creates a new \Model\VendorSchedule object from a given \Model\VendorSpecialDay and weekday
     * 
     * @param \Model\VendorSpecialDay $vendorSpecialDayObject 
     * @return \Model\VendorSchedule
     */
    private function createNewVendorScheduleObject(\Model\VendorSpecialDay $vendorSpecialDayObject): \Model\VendorSchedule {
        $tempVendorScheduleObject = new \Model\VendorSchedule();
        $tempVendorScheduleObject->setVendor_id($vendorSpecialDayObject->getVendorId());
        $tempVendorScheduleObject->setWeekDay($vendorSpecialDayObject->getWeekDay());
        $tempVendorScheduleObject->setAllDay($vendorSpecialDayObject->getAllDay());
        $tempVendorScheduleObject->setStartHour($vendorSpecialDayObject->getStartHour());
        $tempVendorScheduleObject->setStopHour($vendorSpecialDayObject->getStopHour());
        return $tempVendorScheduleObject;
    }

    /**
     * Gets from \Model\VendorSchedule and \Model\VendorSpecialDay Objects arrays 
     * the objects with property values that match the given weekday and vendor_id 
     * 
     * @param array $arrayOfObjects
     * @param int $vendorId
     * @param int $weekday
     * @return array
     */
    private function getObjectsFromArrayByValues(array $arrayOfObjects, int $vendorId, int $weekday): array {
        $items = [];
        foreach ($arrayOfObjects as $struct) {
            if ($vendorId === $struct->getVendorId() && $weekday === $struct->getWeekday()) {
                array_push($items, $struct);
            }
        }
        return $items;
    }

    /**
     * if backup table does not exists creates a new one
     */
    private function handleBackupTable() {
        $backupTableExists = $this->checkBackupTableVendorScheduleExits();
        if ($backupTableExists === FALSE) {
            $this->createBackupTable();
        }
    }

    /**
     * Converts an array of objects into an 2d assoy array
     * 
     * @param array $objectsArray
     * @return array
     */
    private function objectArrayToAssoc(array $objectsArray) {
        $assocArray = [];
        foreach ($objectsArray as $object) {
            array_push($assocArray, $this->objectToAssocArray($object));
        }
        return $assocArray;
    }

    /**
     * Converts any class into an assoc array
     * 
     * @param \stdClass $object
     * @return type
     */
    private function objectToAssocArray($object) {
        $reflectionClass = new \ReflectionClass(get_class($object));
        $array = array();
        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);
            $array[$property->getName()] = $property->getValue($object);
            $property->setAccessible(false);
        }
        return $array;
    }

}
