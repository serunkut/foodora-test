<?php

namespace DAO;

/**
 * DbConnector
 * @author erunkut
 */
class DbConnector extends \PDO implements DatabaseInterface {

    /**
     * PDO instance creation
     *   
     * @param string $dsn dsn string mysql:host=localhost;dbname=test
     * @param string $username db user name
     * @param string $password db user pass
     * @param array $driver_options
     * 
     */
    public function __construct($dsn = DSN, $username = DB_USER_NAME, $password = DB_PASS) {
        parent::__construct($dsn, $username, $password, PDO_OPTIONS);
    }

    /**
     * 
     * Execute an SQL query as prepared statement 
     * if values array is not empty values are binded to the statement before execution 
     * if $fetchMethod is supplied returns all fetched rows
     * 
     * @param string $query 
     * @param array $values optional
     * @param string $fetchMethod optional PDO::FETCH_MODE 
     * @param string $objectType
     * @return array | boolean
     */
    public function executeStatement(string $query, array $values = NULL, $fetchMethod = NULL, $objectType = NULL) {
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' query: ' . $query);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' values: ' . var_export($values, true));
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' fetchMethod: ' . $fetchMethod);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' objectType: ' . $objectType);

        $statementExecutionResult = NULL;
        $statement = $this->prepare($query);
        if (!empty($values)) {
            //$this->bindValues($statement, $values);
            $statementExecutionResult = $statement->execute($values);
        } else {
            $statementExecutionResult = $statement->execute();
        }
        //fetch values into the given object type
        if (!empty($fetchMethod) && $fetchMethod === \PDO::FETCH_CLASS) {
            return $statement->fetchAll($fetchMethod, $objectType);
        } elseif (!empty($fetchMethod) && $fetchMethod === \PDO::FETCH_ASSOC) {
            return $statement->fetchAll($fetchMethod);
        } elseif (!empty($fetchMethod) && $fetchMethod === \PDO::FETCH_COLUMN) {
            return $statement->fetchAll($fetchMethod);
        } else {
            return $statementExecutionResult;
        }
    }

    /**
     * Inserts the data from an 2d assoc array, its prepared only once before the foreach
     * 
     * @param string $query
     * @param array $assocArray2d
     * @return boolean
     */
    public function insertRows(string $query, array $assocArray2d) {
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' query: ' . $query);
        $statement = $this->prepare($query);
        foreach ($assocArray2d as $assocArray1d) {
            foreach ($assocArray1d as $key => $val) {
                $statement->bindValue(':' . $key, $val);
            }
            $statement->execute();
        }
        return TRUE;
    }

    /**
     * Checks if row count is greater than 0 with the given query
     * 
     * @param string $query
     * @return boolean
     */
    public function checkTableHasValues(string $query) {
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' query: ' . $query);
        $statement = $this->prepare($query);
        $statement->execute();
        if ($statement->rowCount() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
