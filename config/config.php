<?php

/**
 * config file for application constants
 * @author selim erunkut
 */

//database constants
DEFINE('DB_HOST', '');
DEFINE('DB_NAME', 'foodora-test');
DEFINE('DB_USER_NAME', '');
//PAssword is definitly not safe here! move it to a safe location (maybe all od config.php)
DEFINE('DB_PASS', '');
DEFINE('DB_CHARSET', 'utf8');
DEFINE("DSN", "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET);
DEFINE("PDO_OPTIONS", array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
));

//backup tabel name
DEFINE('BACKUP_TABLE_NAME','vendor_schedule_backup_special_day_fix');

//logger constants/configuration
DEFINE('LOG_LOCATION', dirname(__DIR__) . '/src/log');
DEFINE('LOG_LEVEL', Log\Psr\Log\LogLevel::DEBUG);


//some predifened text messages
DEFINE('BACKUP_TABLE_CREATED_WITH_DATA',"BACKUP_TABLE_CREATED_WITH_DATA");
DEFINE('BACKUP_TABLE_DELETED',"BACKUP_TABLE_DELETED");
DEFINE('BT_MISSING_REVERT_FAILED', "BT_MISSING_REVERT_FAILED");
DEFINE('REVERT_SUCCESS', "REVERT_SUCCESS");
DEFINE('REVERT_FAILURE',"REVERT_FAILURE");
DEFINE('BACKUP_TABLE_NOT_CREATED',"BACKUP_TABLE_NOT_CREATED");
DEFINE('PATCH_APPLIED',"PATCH_APPLIED");
DEFINE('BACKUP_TABLE_NOT_EXISTS',"BACKUP_TABLE_NOT_EXISTS");
DEFINE('START_DATE', '2015-12-21');
DEFINE('END_DATE', '2015-12-27');