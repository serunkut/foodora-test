<?php

namespace Service;

/**
 * CommandLineService
 * @author erunkut
 */
class CommandLineService {

    private $commandLineParameters;

    public function __construct(array $commandLineParameters) {
        $this->commandLineParameters = $commandLineParameters;
    }

    /**
     * Converts the user CLI inputs into actions and returns them
     * consumes the \Controller\Actions class
     * 
     * @param \Controller\Actions $actions
     * @return \Controller\Actions
     */
    public function checkCliCommands(\Controller\Actions $actions): \Controller\Actions {
        $opt = $this->commandLineParameters;
        if (isset($opt['a'])) {
            if (isset($opt['f'])) {
                $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' cli option: -a and -f');
                //delete and creating the backup table and applying the patch
                $actions->setActions(array($actions::ACTION_DELETE_BACKUP, $actions::ACTION_CREATE_BACKUP, $actions::ACTION_APPLY_PATCH));
                return $actions;
            } else {
                //apply the patch
                $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' cli option: -a');
                $actions->setActions(array($actions::ACTION_APPLY_PATCH));
                return $actions;
            }
        } elseif (isset($opt['r'])) {
            $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' cli option: -r');
            $actions->setActions(array($actions::ACTION_REVERT_PATCH));
            return $actions;
        } else {
            $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . 'no options supplied display help');
            $actions->setActions(array($actions::ACTION_DISPLAY_HELP));
            return $actions;
        }
    }

    /**
     * Catches all unhandled errors that implement the throwable interface
     * @param type $t
     */
    public function handleError($t) {
        $GLOBALS["log"]->critical(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ': Error: ' . $t->getMessage() . "\nTrace: " . $t->getTraceAsString());
        die("Application terminated due to critical error! See log in " . LOG_LOCATION . "\n");
    }

}
