<?php
/**
 * @author erunkut
 */
//autoloader and config must be manually loaded
require_once("config/autoload.php");
require_once("config/config.php");
//initialize logger
$GLOBALS["log"] = new \Log\Logger(LOG_LOCATION, LOG_LEVEL);
//we have no container service, so the database connection, predefined actions and view is instanciated here
$controller = new \Controller\CommandLineController(new \DAO\DbConnector(), new \Controller\Actions(), new \View\MainView());
$controller->initiate();
