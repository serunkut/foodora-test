<?php

namespace DAO;

/**
 * VendorSpecialDayDAO
 * @author erunkut
 */
class VendorSpecialDayDAO {

    /**
     *
     * @var \DAO\DbConnector 
     */
    private $connection;

    public function __construct(\DAO\DatabaseInterface $connection) {
        $this->connection = $connection;
    }

    /**
     * fetches all special days between start and end date
     * and returns them as a an array of \Model\VendorSpecialDay objects
     * 
     * @param string $startDate
     * @param string $endDate
     * @return array of \Model\VendorSpecialDay objects
     */
    public function getVendorSpecialDaysBetwennDates(string $startDate, string $endDate): array {
        $query = "SELECT vendor_id, "
                . "special_date, "
                . "all_day, "
                . "start_hour, "
                . "stop_hour "
                . "FROM vendor_special_day "
                . "WHERE special_date BETWEEN :startDate AND :endDate "
                . "ORDER BY vendor_id ASC";
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' query: ' . $query);
        $parameters = ['startDate' => $startDate, 'endDate' => $endDate];
        return $this->connection->executeStatement($query, $parameters, \PDO::FETCH_CLASS, "\Model\VendorSpecialDay");
    }

    /**
     * deletes the vendor_schedule_backup_special_day_fix table
     * 
     * @throws \Exception if starting the transaction fails
     */
    public function dropVendorSpecialDayBackupTable() {
        $query = "DROP TABLE vendor_schedule_backup_special_day_fix";
        $result = $this->connection->executeStatement($query);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        if ($result !== TRUE) {
            throw new \Exception("dropping vendor_schedule_backup_special_day_fix table failed");
        }
    }

}
