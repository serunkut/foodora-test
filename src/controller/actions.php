<?php

namespace Controller;

/**
 * Actions
 * @author erunkut
 */
class Actions {

    const ACTION_DELETE_BACKUP = "deleteBackup";
    const ACTION_CREATE_BACKUP = "createBackup";
    const ACTION_APPLY_PATCH = "applyPatch";
    const ACTION_REVERT_PATCH = "revertPatch";
    const ACTION_DISPLAY_HELP = "displayHelp";
    const ACTION_CHECK_BACKUP_TABLE_EXISTS = "checkBackupTable";

    private $actions;

    /**
     * Checks if actions are defined and returns them
     * @param array $actions
     * @throws \Exception
     */
    public function setActions(array $actions) {
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' actions: ' . var_export($actions, true));
        if (!array_diff($actions, $this->getConstants())) {
            $this->actions = $actions;
        } else {
            throw new \Exception("action not defined");
        }
    }

    /**
     * Returns an array of \Controller\Actions
     * @return array of \Controller\Actions
     */
    public function getActions(): array {
        return $this->actions;
    }

    /**
     * Returns an array of constants from the created reflection class
     * @return array
     */
    private function getConstants(): array {
        $refl = new \ReflectionClass('\Controller\Actions');
        return $refl->getConstants();
    }

}
