<?php

namespace DAO;

/**
 * VendorScheduleDAO
 * @author erunkut
 */
class VendorScheduleDAO {

    private $connection;

    public function __construct(\DAO\DatabaseInterface $connection) {
        $this->connection = $connection;
    }

    /**
     * database operations for reverting the previously applied patch
     * transaction based part:
     * deletes the vendor_schedule table data
     * inserts data from the backup table
     * commits the transaction and
     * deletes the backup table
     * 
     * if a exception occurs the transaction is rolled back and the exception rethrown
     * 
     * @param \DAO\VendorSpecialDayDAO $vendorSpecialDayDAO
     * @throws \Exception 
     */
    public function revertPatch(\DAO\VendorSpecialDayDAO $vendorSpecialDayDAO) {
        $this->startTransaction();
        try {
            $this->rollbackFriendlyDeleteSchedulesTable();
            $this->insertFromBackupTable();
            $this->endTransaction();
            $vendorSpecialDayDAO->dropVendorSpecialDayBackupTable();
        } catch (\Exception $e) {
            $GLOBALS["log"]->error(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' transaction exception occured, doing a rollback');
            $this->rollback();
            throw $e;
        }
    }

    /**
     * begin the transaction
     * @throws \Exception if starting the transaction fails
     */
    private function startTransaction() {
        $result = $this->connection->beginTransaction();
        if ($result !== TRUE) {
            throw new \Exception("starting transaction failed");
        }
    }

    /**
     * commit a transaction
     * @throws \Exception if commit returns false
     */
    private function endTransaction() {
        $result = $this->connection->commit();
        if ($result !== TRUE) {
            throw new \Exception("Commting the transaction failed");
        }
    }

    /**
     * rollback a transaction
     * @throws \Exception if rollBack returns false
     */
    private function rollback() {
        $result = $this->connection->rollBack();
        if ($result !== TRUE) {
            throw new \Exception("Transaction Rollback failed");
        }
    }

    /**
     * deletes all entries from the vendor_schedule table without truncate
     * 
     * @throws \Exception if statement returns false
     */
    private function rollbackFriendlyDeleteSchedulesTable() {
        $query = "DELETE FROM vendor_schedule";
        $result = $this->connection->executeStatement($query);
        if ($result !== TRUE) {
            throw new \Exception("deleting vendor_schedule table data failed");
        }
    }

    /**
     * creates the backup table vendor_schedule_backup_special_day_fix
     * @throws \Exception if statement returns false
     */
    public function createBackupTableVendorSchedule() {
        $query = "CREATE TABLE vendor_schedule_backup_special_day_fix LIKE vendor_schedule";
        $result = $this->connection->executeStatement($query);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        if ($result !== TRUE) {
            throw new \Exception("creating vendor_schedule_backup_special_day_fix table data failed");
        }
    }

    /**
     * insert all the data into the vendor_schedule_backup_special_day_fix from vendor_schedule
     * @throws \Exception if statement returns false
     */
    public function insertDataIntoBackupTableVendorSchedule() {
        $query = "INSERT INTO vendor_schedule_backup_special_day_fix SELECT * from vendor_schedule";
        $result = $this->connection->executeStatement($query);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        if ($result !== TRUE) {
            throw new \Exception("inserting data into vendor_schedule_backup_special_day_fix table failed");
        }
    }

    /**
     * delete the vendor_schedule_backup_special_day_fix table
     * @throws \Exception if statement returns false
     */
    public function dropBackupTableVendorSchedule() {
        $query = "DROP TABLE vendor_schedule_backup_special_day_fix";
        $result = $this->connection->executeStatement($query);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        if ($result !== TRUE) {
            throw new \Exception("deleting the vendor_schedule_backup_special_day_fix table failed");
        }
    }

    /**
     * fetches and returns vendors_schedule entries as a array of \Model\VendorSchedule objects
     * @return array of \Model\VendorSchedule objects
     */
    public function getVendorSchedules(): array {
        $query = "SELECT "
                . "vendor_id, "
                . "weekday, "
                . "all_day, "
                . "start_hour, "
                . "stop_hour "
                . "FROM vendor_schedule "
                . "ORDER BY vendor_id ASC";
        $result = $this->connection->executeStatement($query, NULL, \PDO::FETCH_CLASS, "\Model\VendorSchedule");
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' count: ' . count($result));
        return $result;
    }

    /**
     * fetches and returns the vendor ids as array
     * 
     * @return array
     */
    public function getVendorIDs(): array {
        $query = "SELECT id FROM vendor";
        $result = $this->connection->executeStatement($query, NULL, \PDO::FETCH_COLUMN);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        return $result;
    }

    /**
     * queries against the vendor_schedule_backup_special_day_fix table to see if its exists 
     * 
     * @return bool TRUE if table exists else FALSE
     */
    public function checkIfBackupTableExists(): bool {
        try {
            $query = "SELECT 1 FROM vendor_schedule_backup_special_day_fix LIMIT 1";
            $result = $this->connection->executeStatement($query);
            $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        } catch (\PDOException $e) {
            //if error code matches table does not exists, 
            //else some other error occured, throw the ecxecption again
            if ($e->getCode() === '42S02') {
                $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . 'backup table does not exit');
                return FALSE;
            } else {
                throw $e;
            }
        }
        //result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return ($result !== FALSE);
    }

    /**
     * Checks if the vendor_schedule_backup_special_day_fix table has any values
     * 
     * @return bool TRUE if table has values else FALSE
     */
    public function checkIfBackupTableHasValues(): bool {
        $query = "SELECT id FROM vendor_schedule_backup_special_day_fix LIMIT 1";
        return $this->connection->checkTableHasValues($query);
    }

    /**
     * Gets the VendorSchedule values as 2d array 
     * removes all data from vendor_schedule
     * inserts them into the database
     * 
     * @param array $sheduleAssocArray
     * @throws \Exception if statement returns false
     */
    public function purgeInsertIntoVendorSchedule(array $sheduleAssocArray) {
        $data = $sheduleAssocArray[0];
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));
        $query = "INSERT INTO vendor_schedule (" . $columnString . ") VALUES (" . $valueString . ")";
        $this->startTransaction();
        try {
            $this->rollbackFriendlyDeleteSchedulesTable();
            $result = $this->connection->insertRows($query, $sheduleAssocArray);
            $this->endTransaction();
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        if ($result !== TRUE) {
            throw new \Exception("inserting data into vendor_schedule table failed");
        }
    }

    /**
     * inserts the backup data from vendor_schedule_backup_special_day_fix table into vendor_schedule
     * 
     * @throws \Exception if statement returns false
     */
    public function insertFromBackupTable() {
        $query = "INSERT INTO vendor_schedule  SELECT * from vendor_schedule_backup_special_day_fix";
        $result = $this->connection->executeStatement($query);
        $GLOBALS["log"]->debug(__CLASS__ . '/' . __FUNCTION__ . '/' . __LINE__ . ' result: ' . var_export($result, true));
        if ($result !== TRUE) {
            throw new \Exception("inserting data from vendor_schedule_backup_special_day_fix into vendor_schedule table failed");
        }
    }

}
